import re
import sys
def is_type_number(num):
    """
    """
    return type(num) == float or type(num) == int

def is_str_num(s:str):
    return re.match("-?(\d+\.?\d*|\.\d+)", s)

def find(callback, iter, value_if_not_found = None):
    for el in iter:
        if callback(el):
            return el
    return value_if_not_found

def cint(num:float):
    """
    convert to int if float number is an integer.
    """
    return int(num) if num.is_integer() else num

def counter(collection):
    c = dict()
    for el in collection:
        if el in c:
            c[el] += 1
        else:
            c[el] = 1
    return c
def exists(fn:callable, collection):
    """
    A functional function that returns true if found something
    """
    for el in collection:
        if fn(el):
            return True
    return False

# Reduce function in python
def reduce(fn:callable, collection, *args):
    arg_len = len(args)
    # check if too many arguments were provided
    if (arg_len > 1):
        raise Exception("Too many arguments in reduce function.")
    it = iter(collection)
    acc = next(it) if arg_len <= 0 else args[0]
    for el in it:
        acc = fn(acc, el)
    return acc

def print_error(*args, **kwargs):
    """
    This function is used to print error messages to the user.
    """
    print(*args, file=sys.stderr, **kwargs)

def all_instance_of(type_class, *objects):
    """
    This function checks if all objects are of the same type and instance of the same class
    If no arguments are provided it returns True
    """
    for o in objects:
        if not isinstance(o, type_class):
            return False
    return True
        
