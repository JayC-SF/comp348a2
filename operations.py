from enum import Enum
class Operation:
    def __init__(self, command, file=False):
        self.__command = command
        self.__file = file
    
    def needs_file(self):
        """
        """
        return self.__file
    
    def get_command(self):
        """
        """
        return self.__command
    
    def __str__(self):
        """
        returns the following if the operation requires a file
        Ex: `SAVE <file>`
        Otherwise it returns the command only.
        """
        return f"{self.__command} "+ ("<file>" if self.__file else "")
    
class Operations(Enum):
    LOAD    = Operation("LOAD", True)
    TOSET   = Operation("TOSET")
    SAVE    = Operation("SAVE", True)
    PRINT   = Operation("PRINT")
    SUMMARY = Operation("SUMMARY")
    DETAILS = Operation("DETAILS")
    HELP    = Operation("HELP")
    QUIT    = Operation("QUIT")

    def find_operation(operation:str):
        """
        """
        for op in Operations:
            if op.value.get_command().casefold() == operation.casefold():
                return op
        return None

