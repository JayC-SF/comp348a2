from shapes.circle import Circle
from shapes.ellipse import Ellipse
from shapes.rhombus import Rhombus
from shapes.shape import Shape
from operations import Operations
from utility import is_str_num, find, reduce, counter, exists, print_error

# create a list of tuples where the numbers are the number of arguments required
shape_classes = [(Shape, 0), (Circle, 1), (Ellipse, 2), (Rhombus, 2)]

def get_menu():
    """
    Returns a string containing the menu of all the commands
    """
    menu = "Available commands to use:\n"
    for op in Operations:
        menu += f"--> {str(op.value)}\n"
    return menu

def main():
    """
    Main function of the shape database program.
    """
    multiset = None
    menu = get_menu()
    is_over = False
    print(menu)
    while(not is_over):
        # print menu and receive next command
        op, file = process_next_input()
        
        # Load the file to the multiset
        if op == Operations.LOAD:
            tmp = load(file)
            # only assign multiset if tmp is not None
            if tmp is not None:
                multiset = tmp
        elif op == Operations.TOSET:
                multiset = toset(multiset)
        elif op == Operations.SAVE:
            save(multiset, file)
        elif op == Operations.PRINT:
            printshapes(multiset)
        elif op == Operations.SUMMARY:
            summary(multiset)
        elif op == Operations.DETAILS:
            details(multiset)
        elif op == Operations.HELP:
            print(menu)
        elif op == Operations.QUIT:
            print("Goodbye!")
            is_over = True
                
def process_next_input():
    """
    This function processes the next input.
    It finds which command corresponds to the inputted command.
    """
    # check if the user inputed something wrong.
    while(True):
        print("Please enter your command here: ", end="")
        user_in = input().split()
        if (len(user_in) == 0):
            continue
        
        op = Operations.find_operation(user_in[0])
        if (op == None):
            print(f"Error: The command `{user_in[0]}` does not exist. Please insert a valid command.")
            continue
        
        # check if the user provides more arguments than required.
        arg_len = 2 if op.value.needs_file() else 1
        if (len(user_in) > arg_len):
            print(f"Error: Too many arguments provided for {op.value.get_command()} command.")
            continue

        # Check if the user is missing a required file
        if (len(user_in) < arg_len):
            print(f"Error: Missing required argument <file> for the command`{op.value.get_command()}`.")
            print("Please include a filename for the command. E.g SAVE my_text.txt")
            continue
        # return the operation in question, and the filename if needed.
        return op, (user_in[1] if op.value.needs_file() else None)

def load(file):
    """
    This function loads a file to process shapes. 
    It performs verbose operations where it will parse the shapes and return all shapes
    that have proper syntax in the file. Should there be any errors it will be printed, 
    but it will not stop the process.
    """
    print(f"Processing `{file}`...")
    shapes = None
    try:
        with open(file, "r") as f:
            shapes = f.read().split('\n')
    except:
        print(f"Error: File `{file}` was not found.")
        return None
    # keep the count of rows
    num_rows = len(shapes)
    # clean the lines by removing all trailing spaces and tokenize the lines
    shapes = map(lambda l: l.strip(), shapes)
    # convert each element to (num, line) tuple in order to keep the line number
    # make the line number start at 1
    shapes = enumerate(shapes, 1)
    # filter out the empty lines, while still keeping the line numbers in the other ones
    shapes = filter(lambda l: l[1] != '', shapes)
    # map each elements to a shape object
    shapes = list(map(lambda e: convert_to_shape(*e), shapes))
    
    # count the None elements using lambda function, pass the shapes and 0 as the starting value
    count_none = lambda acc, el: acc + 1 if el is None else acc
    count_errors = reduce(count_none, shapes, 0)
    # verbose print of the rows, shapes and errors encountered
    print(f"Processed {num_rows} row(s), {len(shapes) - count_errors} shape(s) added, {count_errors} error(s) found.")
    # filter out the None in the list and return a list of shapes without Nones in the list.
    return list(filter(lambda s: s is not None, shapes))
    
# This function takes in a list of elements
# error is thrown if the clean_line is empty
def convert_to_shape(line_number, cleaned_line):
    """
    This function takes a line and returns a shape, if there is an error
    it prints the error with the line number and returns None
    """
    # split the line into tokens
    tokens = cleaned_line.split()   
    # convert to lowercase letters for the shape field
    shape_str = tokens[0].casefold()
    # get the arguments of the line
    args = tokens[1:]
    # find the right shape class for this line
    shape_class_tuple = find(lambda sc: shape_str == sc[0].__name__.casefold(), shape_classes)
    if shape_class_tuple is None:
        print_error(f"Error: Invalid shape name on line {line_number}")
        return None
    # separate the class and the number of required arguments
    shape_class, required_args = shape_class_tuple
    # check if the number of arguments is correct
    if len(args) != required_args:
        print_error(f"Error: Wrong number of arguments on line {line_number}, received {len(args)} when expected {required_args} arguments: {cleaned_line}")
        return None
    # check if the arguments are numbers
    if exists(lambda n: not is_str_num(n), args):
        print_error(f"Error: Argument is not a number on line {line_number}: {cleaned_line}")
        return None
    # convert the arguments to floats and call the constructor
    args = list(map(float, args))
    # Verify that the arguments of the shape are all strictly positive.
    if exists(lambda n: n <= 0, args):
        print_error(f"Error: Non positive argument provided on line {line_number}: {cleaned_line}")
        return None
    # unpack all arguments and call constructor
    return shape_class(*args)

def toset(multiset):
    """
    This function converts a multiset collection to a set. If the collection is already a set or None
    It performs nothing an returns the same input if the multiset is None or already a set type
    """
    if isinstance(multiset, set):
        print("Shapes are already to set.")
    elif multiset is None:
        print("No shapes loaded in memory. No collection converted to set.")
    elif multiset is not None:
        print("Converting shapes collection to a set, removing duplicates...")
        l = len(multiset)
        multiset = set(multiset)
        print(f"Done! Removed {l - len(multiset)} duplicates.")
    return multiset

def save(multiset, file):
    print(f"Saving to details `{file}`...")
    details = map(lambda s: s.detail() + "\n", multiset)
    # add extra return at the end of the file
    try:
        with open(file, "w") as f:
            # write all the lines separated by \n
            f.writelines(details)
            
    except:
        print(f"Error: An error occured while writing to `{file}`.")
        return
    print(f"Successfully saved information to `{file}`.")

def printshapes(multiset):
    """
    This function prints the shapes in a collection
    """
    if multiset is None:
        print("No shape database has been loaded. Please load a file to print their content.")
        return
    if len(multiset) == 0:
        print("0 shapes loaded in memory.")
        return
    for s in multiset:
        s.print()

def summary(multiset):
    """
    Summarizes the numbers of total shapes, Circles, Ellipses, and Rhombuses.
    """
    if multiset is None:
        print("No shapes database has been loaded.")
        multiset = []
    # map each element to their class name
    class_name_list = map(lambda s: s.__class__.__name__, multiset)
    # get a dictionary counting the number of each class name
    c = counter(class_name_list)
    # Print information
    # Use get for the dictionary to not get an error when key doesn't exists
    print(f"Circle(s): {c.get(Circle.__name__, 0)}")
    print(f"Ellipse(s): {c.get(Ellipse.__name__, 0)}")
    print(f"Rhombus(es): {c.get(Rhombus.__name__, 0)}")
    print(f"Shape(s): {len(multiset)}")

def details(multiset):
    """
    Prints the details of a shape object.
    """
    if multiset is None:
        print("No shape database has been loaded. Please load a file to print their details.")
        return
    if len(multiset) == 0:
        print("0 shapes loaded in database.")
        return
    for s in multiset:
        print(s.detail())

if __name__ == "__main__":
    try:
        main()
    except (KeyboardInterrupt, EOFError):
        print("\nGoodbye!")
