from .shape import Shape
from utility import is_type_number, cint, all_instance_of
import math

class Ellipse(Shape):
    def __init__(self, minor_axis, major_axis):
        """
        Constructor function of an Ellipse, it checks if the minor_axis and the major_axis are provided
        properly and without being negative or invalid inputs.
        """
        super().__init__()
        # check if the arguments provided are of type numbers.
        if (not is_type_number(minor_axis) or not is_type_number(major_axis)):
            raise Exception("Error: Argument is not of type number")
        if (minor_axis <= 0 or major_axis <= 0):
            raise Exception("Error: at least one of the arguments is negative")
        # check if need to swap the two.
        if (major_axis < minor_axis):
            tmp = minor_axis
            minor_axis = major_axis
            major_axis = tmp
        # store info
        self.__minor_axis = cint(minor_axis)
        self.__major_axis = cint(major_axis)
        
    def area(self):
        """
        This function returns the area of an ellipse.
        """
        return cint(math.pi * self.__major_axis * self.__minor_axis)
        
    def eccentricity(self):
        """
        This function returns the eccentricity of an ellipse, if any errors occurs it returns None
        """
        try:
            # check if they are equal, if so return 0
            a_squared = self.__major_axis ** 2
            b_squared = self.__minor_axis ** 2
            return cint(math.sqrt(a_squared - b_squared))
        except:
            return None
    
    def detail(self):
        """
        This function computes the detail function of an Ellipse, 
        it returns a string with the format:
        ellipse b a
        where b and a are the minor axis and the major axis respectively
        """
        return f"{Ellipse.__name__.casefold()} {self.__minor_axis} {self.__major_axis}"

    def __hash__(self):
        """
        This function computes the hash value of an ellipse. 
        It uses the attributes and the class name to compute the hash value
        """
        return hash((Ellipse.__name__, self.__major_axis, self.__minor_axis))

    def __eq__(self, other):
        """
        This function checks if the two objects are of the same instance
        """
        is_eq = all_instance_of(Ellipse, self, other)
        is_eq = is_eq and self.__major_axis == other.__major_axis
        is_eq = is_eq and self.__minor_axis == other.__minor_axis
        return is_eq
    
    def __str__(self):
        """
        This function overrides the Shape's to string method. 
        It calls super and adds the eccentricity field in the string.
        """
        e = self.eccentricity()
        e = "undefined" if e is None else e
        return super().__str__() + f", eccentricity: {e}"