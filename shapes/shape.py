from utility import all_instance_of
class Shape:
    """
    Shape class to represent a generic shape.
    """
    # id attribute to be kept for creation of Shape objects
    __id = 1
    
    def __init__(self):
        """
        Constructor function for the Shape class.
        Simply takes the value of current global id as attribute
        and increments the global id.
        """
        self.__id = Shape.__id
        # Increment the shape id for next element creation
        Shape.__id += 1


    # id read only function
    def get_id(self):
        """
        Read only function as a getter for the class
        """
        return self.__id
    
    def perimeter(self):
        """
        Perimeter of the shape
        """
        return None
    
    def area(self):
        """
        Area of the shape
        """
        return None
    
    def print(self):
        """
        Printing function for the shape
        """
        print(self)
    
    def detail(self):
        """
        simply return the classname in lowercase for this class
        """
        return Shape.__name__.casefold()
    
    def __eq__(self, other):
        """
        Checks if the objects are of the same type Shape
        """
        return all_instance_of(Shape, self, other)
    
    def __ne__(self, other):
        """
        Overriding the not equals function, this function will be inherited by other sub
        classes.
        """
        return not self.__eq__(other)
    
    def __hash__(self):
        """
        Hash function for shape class,
        This function hashes the classname
        """
        return hash(Shape.__name__)
    
    def __str__(self):
        """
        Override the parent __str__ function. 
        Prints a shape in the form of:
        
        <id>: <shape>, perimeter: <value1>, area: <value2> 
        
        If None for perimeter, it prints `undefined`
        """
        # we get the perimeter.
        perimeter = self.perimeter()
        # we set perimeter to "undefined" if perimeter is None
        perimeter = "undefined" if perimeter is None else perimeter
        
        # we get the area for the 
        area = self.area()
        area = "undefined" if area is None else area
        
        return f"{self.__id}: {self.__class__.__name__}, perimeter: {perimeter}, area: {area}"