from .shape import Shape
from utility import is_type_number, cint, all_instance_of
import math

class Rhombus(Shape):
    """
    Rhombus shape inheriting from Shape. 
    This class represents a rhombus and supports functions for the shape.
    """
    def __init__(self, p, q):
        """
        This constructor takes two arguments representing the diagonals :p, q. 
        It verifies the type of those arguments and also verifies if the arguments are not 
        negative.
        """
        super().__init__()
        if (not is_type_number(p) or not is_type_number(q)):
            raise Exception("Error: Argument is not of type number")
        if (p <= 0 or q <= 0):
            raise Exception("Error: at least one of the arguments is not positive")
        
        self.__p = cint(p)
        self.__q = cint(q)

    def perimeter(self):
        """
        Computes the perimeter of a rhombus.
        """
        return 4 * self.side()
    
    def area(self):
        """
        This function computes the area of a rhombus. 
        """
        return cint(0.5 * self.__p * self.__q)
    
    def side(self):
        """
        This function computes the side length of a rhombus.
        """
        return cint(0.5 * math.sqrt((self.__p ** 2) + (self.__q ** 2)))
    
    def inradius(self):
        """
        Computes the inradius of the rhombus, and if there are any errors it returns None
        """
        p = self.__p
        q = self.__q
        try:
            return cint(p * q / (2 * math.sqrt((p ** 2) + (q ** 2))))
        except: 
            return None
        
    def detail(self):
        """
        Returns a string with the format of 
        rhombus d1 d2
        where d1 and d2 are the values of the diagonals
        """
        return f"{Rhombus.__name__.casefold()} {self.__p} {self.__q}"

    def __hash__(self):
        """
        Overrides the hash method, uses the diagonals and the class name to compute hash value
        """
        return hash((Rhombus.__name__, self.__p, self.__q))

    def __eq__(self, other):
        """
        Overrides the equals method, checks if objects are of the same instance and type
        and checks if the diagonals are the same
        """
        res = all_instance_of(Rhombus, self, other)
        res = res and self.__p == other.__p
        res = res and self.__q == other.__q
        return res

    def __str__(self):
        """
        This function overrides the tostring method. 
        Instead it uses the previous to string method and adds more functionality to it.
        """
        side = self.side()
        side = "undefined" if side is None else side
        inr = self.inradius()
        inr = "undefined" if inr is None else inr
        return super().__str__() + f", side: {side}, in-radius: {inr}"