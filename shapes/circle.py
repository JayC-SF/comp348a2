from .ellipse import Shape
from utility import is_type_number, cint, all_instance_of
import math

class Circle(Shape):
    def __init__(self, radius):
        # 
        super().__init__()
        if (not is_type_number(radius)):
            raise Exception(f"Error: Argument for {self.__class__.__name__} object has radius not of type number.")
        if (radius <= 0):
            raise Exception("Error: radius argument is not positive")
        self.__radius = cint(radius)

    def perimeter(self):
        """
        """
        # return 2 * pi * r
        return cint(2 * math.pi * self.__radius)
    
    def detail(self):
        """
        simply return `shape`
        Helper function 
        """
        return Circle.__name__.casefold() + f" {self.__radius}"

    def area(self):
        """
        This function defines the area of a circle
        It returns an int or a float depending on the value of the area
        """
        return cint(math.pi * self.__radius ** 2)
    
    def __hash__(self):
        """
        This function overrides the default hash function and performs a hash value 
        based on the name of the class and the value of the radius
        """
        return hash((Circle.__name__, self.__radius))
    
    def __eq__(self, other):
        """
        This function checks if the current and other objects are the same.
        It checks the type of 
        """
        return all_instance_of(Circle, self, other) and self.__radius == other.__radius