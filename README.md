# Comp348a2
## Professor's Instruction Notes 
Using a list for the multiset is acceptable for this assignment. 
Using a dicitonary counter object would've been ideal but not necessary 
for the assignment according to the professor.

Perform a `LOAD` command will simply overwrite the current in-memory database. 
If no in-memory database exist, then it will loaded normally.

No specific id should be kept for the similar shapes. So long 
that all the shapes in the set need to be unique.

This program will read a file with shapes, if there are errors, 
an error is displayed for that specific line, but the program continues
to process the other shapes in the next lines. The result will be that 
only shapes with proper syntax will be processed, and will not. 
This was assumed since in the assignment  the following example is shown:

```
Processed 8 row(s), 7 shape(s) added, 1 error(s).
```

This means that it is expected that the program parses the file completely 
and stores the proper lines while rejecting the bad ones.

All functions required from the assignment are implemented, and all other helper functions were
also approved by the teacher in for the design of the program.

For example, `__hash__`, `__eq__`, `__str__`, (in `Shape` class), `detail`.
The design using `__str__` was in the intention of a simple program design, since it is
good practice to use this function instead of implementing a print function for all classes.

It is assumed that the number of processed rows in load file is the numbers of rows read by the program.